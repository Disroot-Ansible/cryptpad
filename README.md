# Cryptpad - Ansible Role

This role covers deployment, configuration and software updates of Cryptpad. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.
`vagrant up`

`ansible-playbook -b Playbooks/cryptpad.yml`
Add `192.168.33.20    cryptpad.example.org` to `/etc/hosts`
Then you can access cryptpad from your computer on https://cryptpad.example.org

## Customization
You can add your own logo by changing `CryptPad_logo.svg` and  `CryptPad_logo_hero.svg` `files` in the `customize` folder by logos of your choice (but keep those names). (Note: CryptPad_logo_hero logo is used on main page, CryptPad_logo for loading pages).

You can also add/edit files in the `customize` folder as explained on https://docs.cryptpad.org/en/admin_guide/customization.html


## Playbook
The playbook includes nginx and node roles and deploys entire stack needed to run Cryptpad. Additional roles are also available in the Ansible roles repos in git.


## CHANGELOG
- **17.02.2023** -
   - Add customization
- **26.03.2021** -
   - Bumped version to 4.2.1
   - Update readme
   - Add licence
